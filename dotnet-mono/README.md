# dotnet-mono
Docker image for compiling [NET Core](https://www.microsoft.com/net/learn/get-started/windows)
applications on Linux. Includes mono and F# runtime.
